package ru.terrakok.gitlabclient

/**
 * @author Konstantin Tskhovrebov (aka terrakok) on 26.03.17.
 */
object Screens {
    const val AUTH_SCREEN = "oauth screen"
    const val MAIN_SCREEN = "main screen"
    const val PROJECT_SCREEN = "project screen"
    const val ABOUT_SCREEN = "about screen"
}